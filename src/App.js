import React, { Component } from 'react';
import {Input,Button} from 'react-materialize';
import AddScore from './components/AddScore'
import ShowCreateScoreButton from './components/ShowCreateScoreButton';
import ShowVariable from './components/Variables'
import {invokeScoreApi,getAssignedCampaigns,getQueueByCampaignId,saveScore} from './api/service'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export class App extends Component {
  constructor(props){
    super(props);
    this.state={scores:[],campaignList:[],queueList:[]
      ,selectedCampaign:'',selectedQueue:null,isEnabled:false}
    this.updateScores=this.updateScores.bind(this);
    this.updateCampaigns=this.updateCampaigns.bind(this);
    this.updateQueueList=this.updateQueueList.bind(this);
    this.onQueueSelection=this.onQueueSelection.bind(this);
    this.handleCreateClick=this.handleCreateClick.bind(this);
    this.onCampaignSelection=this.onCampaignSelection.bind(this);
    this.addNewScore=this.addNewScore.bind(this);
    this.getAllScore=this.getAllScore.bind(this);
  }
  addNewScore=(payload)=>{
     saveScore(payload,this.getAllScore);  
  }
  updateScores(data){
    this.setState({scores:data});
    if(this.state.selectedQueue !== undefined && this.state.selectedQueue !== '' && this.state.selectedQueue !==null ) {
      this.onQueueSelection(this.state.selectedQueue);
    // console.log('queuuee--',this.state.selectedQueue);
   }
  }
  updateQueueScores=(data)=>{
    this.setState({scores:data});
  }
  updateTargetScoreById=(array)=>{
    console.log('array--',array);
   let data = [...this.state.scores];
    data.forEach((item,index)=>{
      data[index].queue_target_script='';
      data[index].is_selected=false;
       array.forEach(value=>{
        if(parseInt(item.score_id)===parseInt(value.score_id)) {
          data[index].queue_target_script=value.target_script;
          data[index].is_selected=true;
        } 
      })
    })
    this.updateQueueScores(data);
  }
  updateCampaigns(data){
   this.setState({campaignList:data});
   }
  componentDidMount(){
    getAssignedCampaigns(this.updateCampaigns,this.onCampaignSelection);
   }
  updateQueueList(data) {
    this.setState({queueList:data});
   }
  onQueueSelection(queueId) {
    this.setState({selectedQueue:queueId});
    let preScoreState= [...this.state.scores];

    let filterScoreByQueueId= preScoreState.filter(row=>(parseInt(row.context_id)===
    parseInt(queueId) && row.context==='Queue'));
    this.updateTargetScoreById(filterScoreByQueueId)
  }
  getAllScore=(data)=>{
    if(data== undefined){
    } else {
      if(data.status == 200){
        toast.info("Score Configuration add Successfully", {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error("Unable to add Score Configuration", {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
      invokeScoreApi(this.state.selectedCampaign,this.state.selectedQueue,this.updateScores);
  }
  onCampaignSelection(campaignId){  
    this.setState({selectedCampaign:campaignId});
    setTimeout(() => {
     this.getAllScore();
    }, 500);
    getQueueByCampaignId(campaignId,this.updateQueueList);
  }
  handleCreateClick(){
     this.setState({isEnabled:true});
  }
  render() {
    const {campaignList,scores,queueList,selectedQueue,isEnabled} = this.state; 
    let campaignOptions=[];
    campaignOptions.push(<option key="0" value="">Choose an option</option>);
      if(campaignList && campaignList !=='') {
          try{
            campaignList.forEach((item,index) => {
                campaignOptions.push(<option value={item.campaignId} key={item.campaignId} >
                {item.campaignName}</option>);
            });
          } catch(e){}
    }
    let queueOptions=[];
      queueOptions.push(<option key="0" value="">Choose an option</option>);
      if(queueList && queueList !=='') {
          try{
            queueList.forEach((item,index) => {
                queueOptions.push(<option value={item.agentQueueId} key={index}>{item.queueName}</option>);
          });
          }catch(e){

          }
        }
    return (
      <div className="App">
      <ToastContainer autoClose={1000} />
            <div className="row gamification-configuration">
            <div className="col s9 m9 l9 no-padding">
            <div className="gamification-configuration-wrapper">
            <div className="row configure-heading">
            <div className="col s12 m12 l12 ">
              <h3>Configure Scores</h3>
            </div>
          </div>

          <div className="assignment-wrapper row">
              <div className="col s12 m12 l12 campaign-queue-wrapper">
              <div className="campaign-queue-listing">
              <div className="campaign-listing">
              <div className="input-field">
              <Input s={12} m={12} l={12} type='select' id='campaign' value={this.state.selectedCampaign}  onChange={(e)=>{this.onCampaignSelection(e.target.value)}}>
                {campaignOptions}
              </Input>
              <label>Campaign</label>
						</div>
              </div>
              <div className="arrow-icon">
                <i className="zmdi zmdi-chevron-right"></i>
              </div>
              <div className="queue-listing">
              <div className="input-field">
              <Input s={12} m={12} l={12} type='select' id="queue" 
              onChange={(e)=>{this.onQueueSelection(e.target.value)}}
              disabled={scores.length <= 0  ? true : ''}
              >
                {queueOptions}
              </Input>
               <label>Queue</label>
									 </div>
                   </div>
                   </div>
                   </div>
            </div>
            



            {
              scores.length <= 0 && isEnabled===false ? (
               <ShowCreateScoreButton handleCreateClick={this.handleCreateClick} />
              ) : (
               <AddScore scores={scores} 
               selectedCampaign={this.state.selectedCampaign} 
               selectedQueue={this.state.selectedQueue}
               addNewScore={this.addNewScore} 
               />
              )
            }
            </div>
            </div>

            <ShowVariable/>

          </div>




         

        


      </div>
    );
  }
}

