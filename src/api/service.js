
var client=window.AmeyoClient.init();
var apiUrl = window.getApplicationCrmUrl();

function saveScore(payload,callback) {
    let finalUrl = apiUrl + 'game/addScoreConfiguration';
    fetch(finalUrl, {
    method: 'post',
    headers: {'Content-Type':'application/json'},
    body: JSON.stringify(payload),
    }).then(function(response) {
        return response.json();
    }).then((data)=>{
        callback(data);

    }).catch((err)=>{
       console.log('Error:'+err);
    });
}
function invokeScoreApi(campaignId,queueId,callback){
    let finalUrl =apiUrl + 'game/getScoreInfo/'+campaignId+'/';
    fetch(finalUrl, {
    method: 'Get',
    }).then((response)=>{
        return response.json();
    }).then((data)=>{

        callback(data);
    }).catch((err)=>{
        console.log('err',err);
    });

  }
  function getAllVariables(callback){
    let finalUrl =apiUrl + 'game/getstatsVariables';
    fetch(finalUrl, {
    method: 'Get',
    }).then((response)=>{
        return response.json();
    }).then((data)=>{
        callback(data);
    }).catch((err)=>{
        console.log('err',err);
    });
  }
function getAssignedCampaigns(callback,onCampaignSelection){
   // let response=JSON.parse('[{"campaignId":2,"campaignName":"outbound","campaignType":"Outbound Voice Campaign","description":"","processId":1,"contactCenterId":1},{"campaignId":3,"campaignName":"inbound","campaignType":"Interactive Voice Application","description":"","processId":1,"contactCenterId":1},{"campaignId":24,"campaignName":"feedback","campaignType":"Interactive Voice Application","description":"feedback","processId":1,"contactCenterId":1},{"campaignId":23,"campaignName":"IC_new","campaignType":"Interaction Campaign","description":"","processId":4,"contactCenterId":1},{"campaignId":25,"campaignName":"Voice","campaignType":"Interactive Voice Application","description":"Voice","processId":4,"contactCenterId":1},{"campaignId":11,"campaignName":"IC","campaignType":"Interaction Campaign","description":"IC","processId":4,"contactCenterId":1},{"campaignId":15,"campaignName":"feedback","campaignType":"Chat Campaign","description":"feedback","processId":4,"contactCenterId":1},{"campaignId":17,"campaignName":"inbound voice application","campaignType":"Interactive Voice Application","description":"","processId":5,"contactCenterId":1}]');
    //callback(response);
   // onCampaignSelection(response[0].campaignId);
   // ameyorestapi/cc/campaigns/getAllCampaigns
   // client.request.api("getAssignedCampaignsForUser", null)
   var requestObject = {
    url: "ameyorestapi/cc/campaigns/getAllCampaigns",
    headers: {
        "content-type": "application/json"
    },
    method: "GET",
   };
   client.httpRequest.invokeAmeyo(requestObject)
        .then(function(data){
            var assignedCampaigns = JSON.parse(data.response);
            callback(assignedCampaigns);
            onCampaignSelection(assignedCampaigns[0].campaignId);
        }).catch(function(error){
            console.log("getAssignedCampaignsForUser error",error)
        });
}
function getQueueByCampaignId(campaignId,callback) {
    //let response='[{"agentQueueId":24,"campaignId":3,"queueName":"skill","queueType":"resource.request.queue.fifo.type","resourceSchedulerType":"resource.scheduler.skill.based.type","queuePriority":1,"description":"","transferrable":true,"skillIds":[1],"userIdList":[144,143],"dateAdded":1540039405637,"dateModified":1540039405637},{"agentQueueId":19,"campaignId":3,"queueName":"agent5","queueType":"resource.request.queue.fifo.type","resourceSchedulerType":"resource.scheduler.multiple.extension.type","queuePriority":1,"description":"","transferrable":true,"skillIds":[],"userIdList":[92,167,91,89,93],"dateAdded":1539085116984,"dateModified":1539173438652},{"agentQueueId":21,"campaignId":3,"queueName":"agent7","queueType":"resource.request.queue.fifo.type","resourceSchedulerType":"resource.scheduler.multiple.extension.type","queuePriority":1,"description":"","transferrable":true,"skillIds":[],"userIdList":[102,101,99,103],"dateAdded":1539085139138,"dateModified":1539173358502},{"agentQueueId":20,"campaignId":3,"queueName":"agent6","queueType":"resource.request.queue.fifo.type","resourceSchedulerType":"resource.scheduler.multiple.extension.type","queuePriority":1,"description":"","transferrable":true,"skillIds":[],"userIdList":[97,96,94,98],"dateAdded":1539085128070,"dateModified":1539173416922},{"agentQueueId":15,"campaignId":3,"queueName":"agent1","queueType":"resource.request.queue.fifo.type","resourceSchedulerType":"resource.scheduler.multiple.extension.type","queuePriority":1,"description":"","transferrable":true,"skillIds":[],"userIdList":[72,71,69,73],"dateAdded":1539085063686,"dateModified":1539173371664},{"agentQueueId":18,"campaignId":3,"queueName":"agent4","queueType":"resource.request.queue.fifo.type","resourceSchedulerType":"resource.scheduler.multiple.extension.type","queuePriority":1,"description":"","transferrable":true,"skillIds":[],"userIdList":[87,86,84,88],"dateAdded":1539085106238,"dateModified":1539173365256},{"agentQueueId":4,"campaignId":3,"queueName":"qqqqq","queueType":"resource.request.queue.fifo.type","resourceSchedulerType":"resource.scheduler.lru.type","queuePriority":1,"description":"","transferrable":true,"skillIds":[],"userIdList":[6,147,10,7,8],"dateAdded":1537019684381,"dateModified":1538820600132},{"agentQueueId":16,"campaignId":3,"queueName":"agent2","queueType":"resource.request.queue.fifo.type","resourceSchedulerType":"resource.scheduler.multiple.extension.type","queuePriority":1,"description":"","transferrable":true,"skillIds":[],"userIdList":[77,76,74,78],"dateAdded":1539085079050,"dateModified":1539173428760},{"agentQueueId":17,"campaignId":3,"queueName":"agent3","queueType":"resource.request.queue.fifo.type","resourceSchedulerType":"resource.scheduler.multiple.extension.type","queuePriority":1,"description":"","transferrable":true,"skillIds":[],"userIdList":[82,81,79,83],"dateAdded":1539085091516,"dateModified":1539173348163}]';
    //callback(JSON.parse(response));
     
    var requestObject = {
            url: "/ameyorestapi/cc/agentQueues/getByCampaign?campaignId="+campaignId,
            headers: {
                "content-type": "application/json"
            },
            method: "GET",
        };
        client.httpRequest.invokeAmeyo(requestObject).then(function(allQueues){
           // console.log('data---',allQueues.response);
            callback(JSON.parse(allQueues.response));
        }).catch(function(error){
            console.log("all queues api error---",error)
        });
}

  module.exports = {
    invokeScoreApi,
    getAssignedCampaigns,
    getQueueByCampaignId,
    saveScore,
    getAllVariables
  }