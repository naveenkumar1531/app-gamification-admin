import React ,{Component} from 'react'
class ShowCreateScoreButton extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div className="row">
              <div className="col s12 m12 l12 get-started-wrapper">
               <p>Get the competition started</p>
               <button className="btn btn-primary" onClick={this.props.handleCreateClick}>Create Score</button>
              </div>
              </div>
        )
    }
}
export default ShowCreateScoreButton;