import React from 'react'
import brace from 'brace';
import AceEditor from 'react-ace';
import {Input} from 'react-materialize';
const ShowScore=(props)=>{ 
    let queue_target_value=undefined;
    let is_selected=false;
    let score_name="";
    let achived_target="";
    let target="";
    if(props.fields[props.index] !== undefined) {
        let value=props.fields[props.index];
        queue_target_value=(value['queue_target_script'] !== undefined && value['queue_target_script'] !=='' ? value['queue_target_script'] : undefined);    
        is_selected=value['is_selected'] !== undefined ? value['is_selected']:false;
        score_name=value['score_name'];
        achived_target=value['achieved'];
        target=value['target'];
    }
    return (
     <div className="row score-wrapper">
     <div className=" col s11 m11 111">
       <div  className="row">
         <div className="input-field col s4 m4 14">
         <Input s={12} m={12} l={12} type='text' id={`score_name${props.index}`} name={`score_name${props.index}`} 
         value={score_name} 
         onChange={(e)=>{props.updateFieldState(e.target.value,'score_name',props.index)}}
         disabled={props.isQueueSelected}
         />  
                <label htmlFor={`score_name${props.index}`}>Score Name</label>
         </div>
         </div>
         <div className="row mb10">
       <div className="col s6 m6 16">
       <p className="left-align label-info"><label htmlFor="textarea1">Formula (in Javascript)</label></p>
       <div><AceEditor
               mode="javascript"
               theme="github"
               readOnly={props.isQueueSelected}
               value={achived_target}
               onChange={(e)=>{props.updateFieldState(e,'achieved',props.index)}}
               name={`achieved_${props.index}`}
               editorProps={{
                   $blockScrolling: true
               }}
               
           /></div>
         </div>
             <div className=" col s6 m6 16">
             <p className="left-align clearfix  label-info">
             <label htmlFor="textarea1" className="left">Target</label>
             <a className="left">i</a>
             {
                 props.isQueueSelected === true ? (
                    <Input name={`override_target${props.index}`} type='checkbox' label='Override Campaign Settings'
                    checked={is_selected} onClick={(e)=>{
                        props.updateFieldState(e.target.checked,'is_selected',props.index)
                       }
                     }
                    />
                 ) : (
                 <span></span>
                )
             }
             </p>
            <div><AceEditor
               mode="javascript"
               theme="github"
               readOnly={
                queue_target_value !== undefined && props.isQueueSelected === true ? false :
                props.isQueueSelected === false ? false :
                (is_selected === true ? false
                 : true)}
               onChange={(e)=>{props.updateFieldState(e,'target',props.index)}}
               name={`target_${props.index}`}
               value={target}
               editorProps={{
                   $blockScrolling: true
               }}    
             /></div>     
             </div>                
        </div>
         </div>
         <div id={props.index} className="input-field col s1 m1 11 left-align action-btn">
         {
             props.isQueueSelected !== true && props.totalUiScore !==1 ? (
                 <button className="btn btn-delete mr15" onClick={()=>props.handleDeleteBtn(props.index)}><i className="zmdi zmdi-delete "></i></button>   
             ) : (
               <span></span>
             )
         }      
          {
              props.isQueueSelected !== true && props.totalUiScore !==3  && props.totalUiScore === props.rowIndex +1 ? (
                     <button className="btn btn-add" onClick={()=>{props.handleAddBtn(props.index)}}><i className="zmdi zmdi-plus"></i></button>       
                 ) : (
                     <span></span>
                 )
         }
      </div>
   </div>
 )
}
export default ShowScore;