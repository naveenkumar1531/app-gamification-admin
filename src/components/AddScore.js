import React ,{Component} from 'react'
import ShowScore from './ShowScore';
class AddScore extends Component{
    constructor(props){
        super(props);
        this.state ={fields:{}}
        this.handleDeleteBtn=this.handleDeleteBtn.bind(this);
        this.handleSave=this.handleSave.bind(this);
        this.updateFieldState=this.updateFieldState.bind(this);
        this.intializeScoreState=this.intializeScoreState.bind(this);
        this.updateUIFields=this.updateUIFields.bind(this);
       
    }
    componentDidMount(){
        this.intializeScoreState(this.props.scores);
    }
    componentWillReceiveProps(nextProps) {
        if(this.props.scores !== nextProps.scores){
            this.intializeScoreState(nextProps.scores);
        }
    }
    updateUIFields=(fieldObj)=>{
        var fields=Object.assign({},this.state.fields);
        this.setState({fields:fieldObj});
    }
    intializeScoreState=(dbScores)=>{
        let obj={}
        if(dbScores.length <=0){
            // going to initialize default obj for fileds        
            obj[0]={'score_id':'','score_name':'','achieved':'','target':'','is_deleted':'0',
            'context':'Campaign','is_selected':false};
        } else {
            dbScores.forEach((item,index)=>{
                if(item.context==='Campaign') {
                    obj[index]={'score_id':item.score_id,'score_name':item.score_name,
                    'achieved':item.acheived_script,
                    'target':(item.queue_target_script !=='' && item.queue_target_script !== undefined ? item.queue_target_script:item.target_script),
                    'is_deleted':item.is_deleted,
                    'context':item.context,
                    'queue_target_script':item.queue_target_script,
                    'is_selected':item.is_selected
                    };
                }
                
            })
        }
        this.updateUIFields(obj);  
      }
     
    updateFieldState(value,key,index){
        let obj ={};
        let fields = Object.assign({},this.state.fields);
        obj[key] = value;
        if(fields[index] === undefined) {
            fields[index] = obj;    
        } else {
           // fields[index]['queue_target_script']='';
            let newKey = Object.assign({},fields[index],obj);
            fields[index]=newKey;
        }
        this.updateUIFields(fields);
    }
    handleAddBtn=(index)=>{
        let fields = Object.assign({},this.state.fields);
        let nextIndex = parseInt(index)+1;
        fields[nextIndex]={'score_id':'','score_name':'','achieved':'','target':'','is_deleted':'0'
        ,'context':'Campaign','queue_target_script':'','is_selected':false};
        this.updateUIFields(fields);
    }
    handleDeleteBtn=(index)=>{     
           var copy=Object.assign({},this.state.fields);
           let r = copy[index];
           r['is_deleted'] ='1';
           copy[index]=r;      
           this.setState({...this.state.fields=copy});   
    }
    handleSave=()=>{
        let campaignId=this.props.selectedCampaign;
        let queueId=this.props.selectedQueue;
        let fieldData=this.state.fields;
        let payload={}
        payload['campaign_id']=campaignId;
        payload['queue_id']=queueId;
        let data=[];
        Object.keys(fieldData).forEach(item=>{
          let obj ={}
          obj['score_id']=fieldData[item].score_id;
          obj['score_name']=fieldData[item].score_name;
          obj['acheived_script']=fieldData[item].achieved;
          obj['target_script']=fieldData[item].target;
          obj['is_deleted']=fieldData[item].is_deleted;
          obj['is_override']=fieldData[item].is_selected;
          data.push(obj);
        })
        payload['data']=data;
       // console.log('payload----',JSON.stringify(payload));
        this.props.addNewScore(payload);
    }
    handleCancelBtn=()=>{
        this.intializeScoreState(this.props.scores);
    }
    render(){
        let rows=[];
        let rowIndex=0;
        let totalScore = Object.keys(this.state.fields).filter(item=>
            this.state.fields[item].is_deleted==='0'
        )
         Object.keys(this.state.fields).forEach((element,index) => {
            
             if(this.state.fields[element].is_deleted !='0') return;
            rows.push(
              <ShowScore key={element} index={element} rowIndex={rowIndex} 
                data={this.state.fields[element]} 
                totalUiScore={totalScore.length}
                fields={this.state.fields} 
                handleChange={this.handleChange} 
                handleAddBtn={this.handleAddBtn}
                handleDeleteBtn={this.handleDeleteBtn}
                updateFieldState={this.updateFieldState}
                isQueueSelected={this.props.selectedQueue!==null && this.props.selectedQueue!==''  ? true :false}  
                />
               )
               rowIndex++;
        })
         return(
           
            <div>
            {rows} 
            <div className="add-new-wrap-action-wrap row">
            <div className="right-align multiple-btn action-row-wrap no-padding s12 m12 l12 col">
            <button className="btn btn-light" onClick={this.handleCancelBtn}>Cancel</button>
            
            <button className="btn btn-primary" onClick={this.handleSave} 
              disabled={this.props.selectedCampaign===undefined || 
              this.props.selectedCampaign===''  ? true : ''}>Save</button>
            
            </div>
            </div>
            </div>
           )
    }
}
export default AddScore;