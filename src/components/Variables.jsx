import React,{Component} from 'react'
import {getAllVariables} from '../api/service'
import {CopyToClipboard} from 'react-copy-to-clipboard';
const Row=(props)=>{
   let value = props.value;
    return(
      <li><span className="left">{props.value}</span>
      <CopyToClipboard text={value}>
          <button className="btn btn-delete right" ><i className="zmdi zmdi-copy"></i></button>
        </CopyToClipboard>
      </li>
    )
}
class ShowVariable extends Component
{ 
  constructor(props){
    super(props);
    this.state={variables:{}}
    this.callApi=this.callApi.bind();
    this.updateVariable=this.updateVariable.bind();
  } 
  componentDidMount(){
   this.callApi();
  }
  callApi=()=>{
    getAllVariables(this.updateVariable);  
  }
  updateVariable=(data)=>{
    this.setState({variables:data});
  }
  render(){
  let li=[];
  Object.keys(this.state.variables).forEach((element,index) => {
    li.push(<Row key={index} value={element}/>)
  });
  return (
    <div className="col s3 m3 l3 variable-listing no-padding">
    <h3 className=" variable-listing_heading">Variables</h3>
    <div className="row ">
    <div className="col s12 m12 l12 no-padding">
    <ul className="clearfix">
      {li}    
    </ul>
  </div>
  </div>
  </div>
  )
 }
}
export default ShowVariable;