
      $(document).ready(function(){
     //   $('select').material_select();
       // $('.tooltipped').tooltip();
        $('.read-more').click(function(){
            //alert(0);
            $(this).parent().find(".more-detail-description").show();
        });
        $('.close-detail-content').click(function(){
            $(this).parents().find(".more-detail-description").hide();
        });

           $(function() {
         	Highcharts.setOptions({
         		colors: ['#3597D3', '#E53935', '#FBCB43', '#4CAF50']
         	});
         	$('#container').highcharts({
         		chart: {
         			type: 'pie'
         		},
				
         		title: {
         			text: '',
         			style: {
         				fontWeight: 'normal',
         				fontFamily: 'OpenSans'
         			}
         		},
         		plotOptions: {
         			pie: {
         				innerSize: 80,
						 
         			}
					
         		},
         		exporting: {
         			enabled: false
         		},
         		legend: {
         			layout: 'vertical',
         			verticalAlign: 'top',
					itemMarginTop: 3,
         			itemStyle: {
         				fontWeight: 'normal',
						fontSize: '12px',
						fontFamily: 'OpenSans'
         			},
         			enabled: false,
         			floating: true,
         			align: 'right',
         			y: 100,
         			x: -10
         		},
         		series: [{
         			dataLabels: {
         				enabled: false
         			},
         			showInLegend: true,
         			data: [
         				{y:49.9,color:'#f89422'}, {y:71.5,color:'#8cc641'}
         				
         			]
         		}]
         	});
         })

		 //chart 1 ends here
		 $(function () {
    
    Highcharts.setOptions({
     colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4']
    });
    var chart;

    $(document).ready(function() {

        chart = new Highcharts.Chart({

            chart: {

                renderTo: '#container',

                plotBackgroundColor: null,

                plotBorderWidth: null,

                plotShadow: false

            },

            title: {

                text: 'Browser market shares at a specific website, 2010'

            },

            tooltip: {

                formatter: function() {

                    return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';

                }

            },

            plotOptions: {

                pie: {

                    allowPointSelect: true,

                    cursor: 'pointer',

                    dataLabels: {

                        enabled: true,

                        color: '#000000',

                        connectorColor: '#000000',

                        formatter: function() {

                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';

                        }

                    }

                }

            },

            series: [{

                type: 'pie',

                name: 'Browser share',

                data: [

                    ['Firefox',   45.0],

                    ['IE',       26.8],

                    {

                        name: 'Chrome',

                        y: 12.8,

                        sliced: true,

                        selected: true

                    },

                    ['Safari',    8.5],

                    ['Opera',     6.2],

                    ['Others',   0.7]

                ]

            }]

        });

    });
});

		 
		 
		 
		 
		 
		//chart 2 ends here
		 
		 
      });
   